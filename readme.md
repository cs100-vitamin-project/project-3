# Project 3 - Magic Random

![image](demoPlay.gif)

You may have seen random somewhere else. Some people may wonder: computation is something accurate, why we need random on programming (except game programming)? This time, you will see how random play a vital role in Artificial Intelligence and achieve something amazing.

After this project, you should:
- know how random is working.
- know basic usage of `<vector>`
- know how to use module from others
- build a simple AI for 2048

The whole project is based on game 2048. If you are not familiar with this popular game, please refer to [this website](https://play2048.co//).

---
## Files

There are 3 tasks in total.

Files to modify:

    Agents.cpp

Files to read:

    Agents.hpp
    State.cpp
    State.hpp
    main.cpp

Files that should not be modified:

    platform.cpp
    platform.hpp

---
## Task 1 - Random Agent

In this task, you should implement the function `char RandomAgent::getAction(State gameState)` in `Agents.cpp`.

You should randomly return a possible action in the current game state.

There might be some member functions of class `State` that are useful to you:
- [ `std::vector<char> State::getActions()` ] This member function returns a vector of char, each char represents an action that is valid in this game state.

Go to `State.cpp` for more description.

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp -o main

in the console to compile, and get `main` or `main.exe`.

Then run (for Windows cmd)

    main.exe -T 1

to see the result. To test your implementation, run (for Windows cmd)

    main.exe -T 1 -s 5

If you implement it correctly, you should end up with

```cmd

    SCORE: 2380         MAX: 256

    +------+------+------+------+
    |8     |2     |4     |2     |
    +------+------+------+------+
    |2     |4     |256   |32    |
    +------+------+------+------+
    |4     |64    |32    |16    |
    +------+------+------+------+
    |16    |2     |4     |2     |
    +------+------+------+------+

```

---
## Task 2 - Play a game

In this task, you should implement the function `int MonteCarloAgent::randomlyPlayGameOnce(State gameState)` in `Agents.cpp`.

From game state, simulate the game process and get the total score. You should play the game randomly until you reach the dead state. Just repeat randomly picking an action and adding a tile. For each action, take down the score you get. When the game is over, add the scores up and return it.

There might be some member functions of class `State` that are useful to you:
- [ `std::vector<char> State::getActions()` ] This member function returns a vector of char, each char represents an action that is valid in this game state.
- [ `State State::getSuccessor(char action)` ] This member function returns a new State which is the next state after taking the action from original state.
- [ `State State::getSuccessor(char action, int *score)` ] This member function returns a new State which is the next state after taking the action from original state. The
  *score will be assigned as the score got by this action.
- [ `State State::getAddTileSuccessor()` ] This member function returns a new State after randomly adding a tile 2 or 4, with the same probability as the real game.
- [ `bool State::isDeadState()` ] This function returns true if current state is dead. i.e. the game is over.

Go to `State.cpp` for more description.

**You should not use class `Game` in your implementation.**

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp -o main

in the console to compile, and get `main` or `main.exe`.

To test your implementation, run (for Windows cmd)

    main.exe -T 2 -s 5

If you implement it correctly, you should end up with

```cmd
The score of this random game is 2380
```

---
## Task 3 - Monte Carlo Agent

In this task, you should implement the function `char MonteCarloAgent::getAction(State gameState)` in `Agents.cpp`.

Time to build your own real AI!

For Monte Carlo Agent, you should
1. From current game state, take a valid action.
2. After taking the action, play the game randomly for many times. Here you should use `int MonteCarloAgent::getTrialTime()` to get the trial time you need.
3. Repeat step 1 for many times until you have tried all the possible actions.
4. Return the action that get the highest average score during your random play.

There might be some member functions of class `State` that are useful to you:
- [ `std::vector<char> State::getActions()` ] This member function returns a vector of char, each char represents an action that is valid in this game state.
- [ `State State::getSuccessor(char action)` ] This member function returns a new `State` which is the next state after taking the action from original state.
- [ `State State::getSuccessor(char action, int *score)` ] This member function returns a new `State` which is the next state after taking the action from original state. The `*score` will be assigned as the score got by this action.
- [ `State State::getAddTileSuccessor()` ] This member function returns a new `State` after randomly adding a tile 2 or 4, with the same probability distribution as the real game.

Go to `State.cpp` for more description.

When you finish, type

    g++ main.cpp platform.cpp State.cpp Agents.cpp -o main

in the console to compile, and get `main` or `main.exe`.

Then run (for Windows cmd)

    main.exe -T 3

to see the result. It may take some time. To test your implementation, run (for Windows cmd)

    main.exe -T 3 -s 100

If you implement it correctly, you should end up with

```cmd

    SCORE: 75820        MAX: 4096

    +------+------+------+------+
    |4     |2     |2048  |8     |
    +------+------+------+------+
    |16    |64    |4     |2     |
    +------+------+------+------+
    |32    |256   |128   |16    |
    +------+------+------+------+
    |1024  |16    |4096  |2     |
    +------+------+------+------+

```

If your result is better than me, that is okay. Your implementation may be more complicated than me. But if you get a result such as the MAX is just 1024, there might be little bugs in your program.

You can find HINTs in `Agents.cpp`.